package com.example.demo.person.controller;

import com.example.demo.person.model.PersonDto;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PersonControllerIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    @Order(1)
    void getPersons() throws Exception{
        ResponseEntity<PersonDto[]> response = testRestTemplate.getForEntity("/persons", PersonDto[].class);

        assertEquals(200, response.getStatusCodeValue());

        PersonDto[] personDtos = response.getBody();
        assertEquals(10, personDtos.length);
        assertEquals("Peter", personDtos[1].getName());
        assertEquals("Petersen", personDtos[1].getLastname());
        assertEquals("18439", personDtos[1].getZipcode());
        assertEquals("Stralsund", personDtos[1].getCity());
        assertEquals("grün", personDtos[1].getColor());
    }

    @Test
    @Order(2)
    void getPersonById() {
        ResponseEntity<PersonDto> response = testRestTemplate.getForEntity("/person/id/2", PersonDto.class);

        assertEquals(200, response.getStatusCodeValue());

        PersonDto personDto = response.getBody();
        assertEquals("Peter", personDto.getName());
        assertEquals("Petersen", personDto.getLastname());
        assertEquals("18439", personDto.getZipcode());
        assertEquals("Stralsund", personDto.getCity());
        assertEquals("grün", personDto.getColor());


        response = testRestTemplate.getForEntity("/person/id/22222", PersonDto.class);

        assertEquals(204, response.getStatusCodeValue());
        response.getHeaders().forEach((key, value) -> {
            if(key.equals("message")) {
                assertEquals("Eine Person mit dieser ID wurde nicht gefunden.", value.get(0));
            }
        });

    }

    @Test
    @Order(3)
    void getPersonByName() {
        ResponseEntity<PersonDto[]> response = testRestTemplate.getForEntity("/person/name/Peter", PersonDto[].class);

        assertEquals(200, response.getStatusCodeValue());

        PersonDto[] personDtos = response.getBody();
        assertEquals(1, personDtos.length);
        assertEquals("Peter", personDtos[0].getName());
        assertEquals("Petersen", personDtos[0].getLastname());
        assertEquals("18439", personDtos[0].getZipcode());
        assertEquals("Stralsund", personDtos[0].getCity());
        assertEquals("grün", personDtos[0].getColor());


        response = testRestTemplate.getForEntity("/person/name/Peterrrrrrrrrrr", PersonDto[].class);

        assertEquals(200, response.getStatusCodeValue());

        personDtos = response.getBody();
        assertEquals(0, personDtos.length);
    }

    @Test
    @Order(4)
    void getPersonByColor() {
        ResponseEntity<PersonDto[]> response = testRestTemplate.getForEntity("/person/color/grün", PersonDto[].class);

        assertEquals(200, response.getStatusCodeValue());

        PersonDto[] personDtos = response.getBody();
        assertEquals(3, personDtos.length);


        response = testRestTemplate.getForEntity("/person/color/grünnnnnn", PersonDto[].class);

        assertEquals(200, response.getStatusCodeValue());

        personDtos = response.getBody();
        assertEquals(0, personDtos.length);
    }

    @Test
    @Order(5)
    void createPerson() {
        PersonDto personDto = new PersonDto("TestLastname", "TestName", "TestZipcode", "TestCity", "TestColor");
        HttpEntity<PersonDto> request = new HttpEntity<>(personDto);
        ResponseEntity<PersonDto> response = testRestTemplate.postForEntity("/person", request, PersonDto.class);

        assertEquals(200, response.getStatusCodeValue());

        personDto = response.getBody();
        assertEquals("TestName", personDto.getName());
        assertEquals("TestLastname", personDto.getLastname());
        assertEquals("TestZipcode", personDto.getZipcode());
        assertEquals("TestCity", personDto.getCity());
        assertEquals("TestColor", personDto.getColor());


        ResponseEntity<PersonDto[]> response2 = testRestTemplate.getForEntity("/persons", PersonDto[].class);

        assertEquals(200, response2.getStatusCodeValue());

        PersonDto[] personDtos = response2.getBody();
        assertEquals(11, personDtos.length);
    }

    @Test
    @Order(6)
    void updatePerson() {
        PersonDto personDto = new PersonDto(1L, "TestLastname", "TestName", "TestZipcode", "TestCity", "TestColor");
        HttpEntity<PersonDto> request = new HttpEntity<>(personDto);
        ResponseEntity<PersonDto> response = testRestTemplate.exchange("/person", HttpMethod.PUT, request, PersonDto.class);

        assertEquals(200, response.getStatusCodeValue());

        personDto = response.getBody();
        assertEquals(1L, personDto.getId());
        assertEquals("TestName", personDto.getName());
        assertEquals("TestLastname", personDto.getLastname());
        assertEquals("TestZipcode", personDto.getZipcode());
        assertEquals("TestCity", personDto.getCity());
        assertEquals("TestColor", personDto.getColor());

        ResponseEntity<PersonDto[]> response2 = testRestTemplate.getForEntity("/persons", PersonDto[].class);

        assertEquals(200, response2.getStatusCodeValue());

        PersonDto[] personDtos = response2.getBody();
        assertEquals(11, personDtos.length);

        personDto = new PersonDto(100L, "TestLastname", "TestName", "TestZipcode", "TestCity", "TestColor");
        request = new HttpEntity<>(personDto);
        response = testRestTemplate.exchange("/person", HttpMethod.PUT, request, PersonDto.class);
        assertEquals(204, response.getStatusCodeValue());
        assertEquals(null, response.getBody());
    }

    @Test
    @Order(7)
    void deletePerson() {
        ResponseEntity<PersonDto> response = testRestTemplate.exchange("/person/1", HttpMethod.DELETE, new HttpEntity<>(null), PersonDto.class);

        ResponseEntity<PersonDto[]> response2 = testRestTemplate.getForEntity("/persons", PersonDto[].class);



        PersonDto[] personDtos = response2.getBody();
        assertEquals(10, personDtos.length);


        response = testRestTemplate.exchange("/person/100", HttpMethod.DELETE, new HttpEntity<>(null), PersonDto.class);
        assertEquals(204, response.getStatusCodeValue());

        response2 = testRestTemplate.getForEntity("/persons", PersonDto[].class);


        personDtos = response2.getBody();
        assertEquals(10, personDtos.length);
    }
}