package com.example.demo.person.service;

import com.example.demo.person.model.PersonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PersonCsvServiceImplUnitTest {

    @Test
    void getPersons() {
        // check, if method returns 10 PersonDtos
        PersonCsvServiceImpl personCsvService = new PersonCsvServiceImpl(); // Arrange
        List<PersonDto> personList = personCsvService.getPersons(); // Act
        assertEquals(10, personList.size()); // Assert
    }

    @Test
    void getPersonById() {
        // check, if method return 1 PersonDto
        PersonCsvServiceImpl personCsvService = new PersonCsvServiceImpl(); // Arrange
        PersonDto personDto = personCsvService.getPersonById(1L); // Act
        assertNotEquals(personDto, null); // Assert
        // check, if method returns null, as the id doesn't exist
        personDto = personCsvService.getPersonById(11L); // Act
        assertEquals(personDto, null); // Assert
    }

    @Test
    void getPersonsByName() {
        // check, if method returns a List with 1 PersonDto
        PersonCsvServiceImpl personCsvService = new PersonCsvServiceImpl(); // Arrange
        List<PersonDto> personDtos = personCsvService.getPersonsByName("Jonas"); // Act
        assertEquals(1, personDtos.size()); // Assert
        // check, if method returns a List with 0 PersonDto
        personDtos = personCsvService.getPersonsByName("OhneNamen"); // Act
        assertEquals(0, personDtos.size()); // Assert
    }

    @Test
    void getPersonsByColorName() {
        PersonCsvServiceImpl personCsvService = new PersonCsvServiceImpl(); // Arrange
        // check, if method returns a list of 2 PersonDtos
        List<PersonDto> personDtos = personCsvService.getPersonsByColorName("blau"); // Act
        assertEquals(2, personDtos.size()); // Assert
        // check, if method returns a list of 3 PersonDtos
        personDtos = personCsvService.getPersonsByColorName("grün"); // Act
        assertEquals(3, personDtos.size()); // Assert
        // check, if method returns a list of 1 PersonDto
        personDtos = personCsvService.getPersonsByColorName("rot"); // Act
        assertEquals(1, personDtos.size()); // Assert
        // check, if method returns a list of 0 PersonDto
        personDtos = personCsvService.getPersonsByColorName("weiß"); // Act
        assertEquals(0, personDtos.size()); // Assert
        // check, if method returns a list of 0 PersonDto
        personDtos = personCsvService.getPersonsByColorName("schwarz"); // Act
        assertEquals(0, personDtos.size()); // Assert
    }

    @Test
    void createPerson() {
        // check if method returns a list of 7 colors
        PersonCsvServiceImpl personCsvService = new PersonCsvServiceImpl(); // Arrange
        assertEquals(7, personCsvService.getColorCsvRepository().findAll().size()); // Assert
        // check if method returns a list of 11 PersonDtos and 8 colors
        PersonDto personDto = new PersonDto("testname", "testlastname", "12345", "testcity", "blau2"); // Act
        personDto = personCsvService.createPerson(personDto);
        assertEquals(11, personCsvService.getPersons().size()); // Assert
        assertEquals(8, personCsvService.getColorCsvRepository().findAll().size());
        // check if new PersonDto has color blau2
        assertEquals("blau2", personDto.getColor());
        // check if method returns a list of 12 PersonDtos and 8 colors
        personDto = new PersonDto("testname", "testlastname", "12345", "testcity", "blau"); // Act
        personDto = personCsvService.createPerson(personDto);
        assertEquals(12, personCsvService.getPersons().size()); // Assert
        assertEquals(8, personCsvService.getColorCsvRepository().findAll().size());
        // check if new PersonDto has color blau
        assertEquals("blau", personDto.getColor());
    }

    @Test
    void updatePerson() {
        // check, if method returns 7 Colors
        PersonCsvServiceImpl personCsvService = new PersonCsvServiceImpl(); // Arrange
        assertEquals(7, personCsvService.getColorCsvRepository().findAll().size()); // Assert
        // check, if method returns 10 PersonDtos and still 7 Colors
        PersonDto personDto = new PersonDto(1L, "testname", "testlastname", "12345", "testcity", "rot"); // Act
        personDto = personCsvService.updatePerson(personDto);
        assertEquals(10, personCsvService.getPersons().size()); // Assert
        assertEquals(7, personCsvService.getColorCsvRepository().findAll().size());
        assertEquals("rot", personDto.getColor());
        // check, if method return 2 PersonDto, that have favourite color rot
        assertEquals(2, personCsvService.getPersonsByColorName("rot").size());

        // check, if ...
        personDto = new PersonDto(1L, "testname", "testlastname", "12345", "testcity", "blau2"); // Act
        personDto = personCsvService.updatePerson(personDto);
        assertEquals(10, personCsvService.getPersons().size()); // Assert
        assertEquals(8, personCsvService.getColorCsvRepository().findAll().size());
        assertEquals("blau2", personDto.getColor());
        assertEquals(1, personCsvService.getPersonsByColorName("blau2").size());
    }

    @Test
    void deletePerson() {
        // check, if ...
        PersonCsvServiceImpl personCsvService = new PersonCsvServiceImpl(); // Arrange

        PersonDto personDto = personCsvService.deletePerson(1L); // Act
        assertEquals(personDto, null); // Assert
        assertEquals(9, personCsvService.getPersons().size()); // Assert

        personDto = personCsvService.deletePerson(11L); // Act
        assertNotEquals(personDto, null); // Assert
        assertEquals(9, personCsvService.getPersons().size()); // Assert
    }
}