package com.example.demo;

import com.example.demo.color.model.Color;
import com.example.demo.color.repository.ColorRepository;
import com.example.demo.person.model.Person;
import com.example.demo.person.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyRunner implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(MyRunner.class);

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ColorRepository colorRepository;

    @Override
    public void run(String... args) throws Exception {


        Color colorBlue = new Color("blau");
        colorRepository.save(colorBlue);
        Color colorGreen = new Color("grün");
        colorRepository.save(colorGreen);
        Color colorViolet = new Color("violett");
        colorRepository.save(colorViolet);
        Color colorRed = new Color("rot");
        colorRepository.save(colorRed);
        Color colorYellow = new Color("gelb");
        colorRepository.save(colorYellow);
        Color colorTurquoise = new Color("türkis");
        colorRepository.save(colorTurquoise);
        Color colorWhite = new Color("weiß");
        colorRepository.save(colorWhite);

        personRepository.save(new Person("Hans", "Müller", "67742", "Lauterecken", colorBlue));
        personRepository.save(new Person("Peter", "Petersen", "18439", "Stralsund", colorGreen));
        personRepository.save(new Person("Johnny", "Johnson", "88888", "made up",colorViolet));
        personRepository.save(new Person("Milly", "Millenium", "77777", "made up too",colorRed));
        personRepository.save(new Person("Jonas", "Müller", "32323", "Hansstadt",colorYellow));
        personRepository.save(new Person("Tastatur", "Fujitsu", "42342", "Japan",colorTurquoise));
        personRepository.save(new Person("Anders", "Andersson", "32132", "Schweden",colorGreen));
        personRepository.save(new Person("Bertram", "Bart", "12313", "Wasweißich", colorBlue));
        personRepository.save(new Person("Gerda", "Gerber", "76535", "Woanders", colorViolet));
        personRepository.save(new Person("Klaus", "Klaussen", "43246", "Hierach", colorGreen));

        logger.info("# of cities: {}", personRepository.count());

        logger.info("All persons unsorted:");
        List<Person> persons = personRepository.findAll();
        logger.info("{}", persons);
    }
}
