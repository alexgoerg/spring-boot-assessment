package com.example.demo.color.repository;

import com.example.demo.color.model.Color;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ColorCsvRepository {

    List<Color> colors = new ArrayList<Color>();

    public ColorCsvRepository() {
        this.colors = new ArrayList<>();
        Color color = new Color(1L, "blau");
        this.colors.add(color);
        color = new Color(2L, "grün");
        this.colors.add(color);
        color = new Color(3L, "violett");
        this.colors.add(color);
        color = new Color(4L, "rot");
        this.colors.add(color);
        color = new Color(5L, "gelb");
        this.colors.add(color);
        color = new Color(6L, "türkis");
        this.colors.add(color);
        color = new Color(7L, "weiß");
        this.colors.add(color);
    }

    public List<Color> findAll() {
        return this.colors;
    }

    public Color findById(Long id) {
        for (Color color : this.colors) {
            if(color.getId() == id) {
                return color;
            }
        }
        return null;
    }

    public Color findColorByName(String name) {
        for (Color color : this.colors) {
            if(color.getName().equals(name)) {
                return color;
            }
        }
        return null;
    }

    public void save(String name) {
        Color colorToHandle = this.findColorByName(name);
        if(colorToHandle == null) {
            Color colorToCreate = new Color(Long.valueOf((this.colors.size() + 1)), name);
            this.colors.add(colorToCreate);
        }

    }
}
