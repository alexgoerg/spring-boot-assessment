package com.example.demo.person.service;

import com.example.demo.color.model.Color;
import com.example.demo.color.repository.ColorRepository;
import com.example.demo.person.model.Person;
import com.example.demo.person.model.PersonDto;
import com.example.demo.person.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ColorRepository colorRepository;

    @Override
    @Transactional(readOnly = true)
    public List<PersonDto> getPersons() {
        List<Person> persons = this.personRepository.findAll();
        return this.mapToListDto(persons);
    }

    @Override
    @Transactional(readOnly = true)
    public PersonDto getPersonById(Long id){
        Person person = this.personRepository.findById(id).orElse(null);
        if(person == null) {
            return null;
        } else {
            return this.mapToDto(person);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<PersonDto> getPersonsByName(String name) {
        List<Person> persons = this.personRepository.findPersonByName(name);
        return this.mapToListDto(persons);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PersonDto> getPersonsByColorName(String name) {
        Color color = this.colorRepository.findColorByName(name);
        List<Person> persons = this.personRepository.findPersonByColor(color);
        return this.mapToListDto(persons);
    }

    @Override
    @Transactional
    public PersonDto createPerson(PersonDto personDto) {
        // check if color exists
        Color color = this.colorRepository.findColorByName(personDto.getColor());
        // if not, then create the color
        if(color == null) {
            Color colorToCreate = new Color(personDto.getColor());
            this.colorRepository.save(colorToCreate);
        }
        // check transaction management
        // System.out.println(10/0);
        // map
        Person person = this.mapToPerson(personDto);
        // create
        this.personRepository.save(person);
        // evaluate
        Person personCreated = this.personRepository.findById(person.getId()).orElse(null);
        if(personCreated == null) {
            return null;
        } else {
            // map
            return this.mapToDto(personCreated);
        }
    }

    @Override
    @Transactional
    public PersonDto updatePerson(PersonDto personDto) {
        // Check if color exists
        Color color = this.colorRepository.findColorByName(personDto.getColor());
        // If not, then create the color
        if(color == null) {
            Color colorToCreate = new Color(personDto.getColor());
            this.colorRepository.save(colorToCreate);
        }
        // Update
        Person personToUpdate = this.personRepository.findById(personDto.getId()).orElse(null);
        if(personToUpdate == null) {
            return null;
        }
        // check transaction management
        // System.out.println(10/0);
        personToUpdate.setName(personDto.getName());
        personToUpdate.setLastname(personDto.getLastname());
        personToUpdate.setZipcode(personDto.getZipcode());
        personToUpdate.setCity(personDto.getCity());
        personToUpdate.setColor(this.colorRepository.findColorByName(personDto.getColor()));
        this.personRepository.save(personToUpdate);
        // Evaluate
        Person personUpdated = this.personRepository.findById(personDto.getId()).orElse(null);
        PersonDto personDtoUpdated = this.mapToDto(personUpdated);
        if(personDto.equals(personDtoUpdated)) {
            return personDtoUpdated;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public PersonDto deletePerson(Long id) {
        // Delete
        Person personToDelete = this.personRepository.findById(id).orElse(null);
        if(personToDelete == null) {
            return new PersonDto();
        }
        this.personRepository.deleteById(id);
        // Evaluate
        Person personDeleted = this.personRepository.findById(id).orElse(null);
        if(personDeleted != null) {
            return this.mapToDto(personDeleted);
        } else {
            return null;
        }
    }

    private PersonDto mapToDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.setId(person.getId());
        personDto.setName(person.getName());
        personDto.setLastname(person.getLastname());
        personDto.setZipcode(person.getZipcode());
        personDto.setCity(person.getCity());
        personDto.setColor(person.getColor().getName());
        return personDto;
    }

    private Person mapToPerson(PersonDto personDto) {
        Person person = new Person();
        person.setId(personDto.getId());
        person.setName(personDto.getName());
        person.setLastname(personDto.getLastname());
        person.setZipcode(personDto.getZipcode());
        person.setCity(personDto.getCity());
        person.setColor(this.colorRepository.findColorByName(personDto.getColor()));
        return person;
    }

    private List<PersonDto> mapToListDto(List<Person> people) {
        List<PersonDto> personDtos = new ArrayList<>();
        PersonDto personDto;
        for (Person person: people) {
            personDto = this.mapToDto(person);
            personDtos.add(personDto);
        }
        return personDtos;
    }


}
