package com.example.demo.person.service;

import com.example.demo.person.model.Person;
import com.example.demo.person.model.PersonDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public interface PersonService {
    public List<PersonDto> getPersons();

    public PersonDto getPersonById(Long id);

    public List<PersonDto> getPersonsByName(String name);

    public List<PersonDto> getPersonsByColorName(String name);

    public PersonDto createPerson(PersonDto personDto);

    public PersonDto updatePerson(PersonDto person);

    public PersonDto deletePerson(Long id);
}
