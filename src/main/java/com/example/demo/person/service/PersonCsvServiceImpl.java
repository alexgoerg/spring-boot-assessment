package com.example.demo.person.service;

import com.example.demo.color.model.Color;
import com.example.demo.color.repository.ColorCsvRepository;
import com.example.demo.person.model.Person;
import com.example.demo.person.model.PersonDto;
import com.example.demo.person.repository.PersonCsvRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class PersonCsvServiceImpl implements PersonService {

    @Autowired
    private PersonCsvRepository personCsvRepository = new PersonCsvRepository();

    @Autowired
    private ColorCsvRepository colorCsvRepository = new ColorCsvRepository();

    private List<Person> persons = new ArrayList<>();

    public PersonCsvServiceImpl() {
        this.persons = this.personCsvRepository.findAll();
    }

    public ColorCsvRepository getColorCsvRepository() {
        return colorCsvRepository;
    }

    @Override
    public List<PersonDto> getPersons() {
        List<PersonDto> personDtos = this.mapToListDto(this.persons);
        return personDtos;
    }

    @Override
    public PersonDto getPersonById(Long id) {
        Person person;
        PersonDto personDto;
        try {
            person = this.persons
                    .stream()
                    .filter(p -> p.getId().equals(id))
                    .findFirst()
                    .get();
            personDto = this.mapToDto(person);
        } catch (NoSuchElementException noSuchElementException) {
            System.out.println(noSuchElementException.getMessage());
            personDto = null;
        }
        return personDto;
    }

    @Override
    public List<PersonDto> getPersonsByName(String name) {
        List<Person> personList;
        List<PersonDto> personDtos = new ArrayList<>();
        try {
            personList = this.persons
                    .stream()
                    .filter(p -> p.getName().equals(name))
                    .collect(Collectors.toList());
            personDtos = this.mapToListDto(personList);
        } catch (NoSuchElementException noSuchElementException) {
            System.out.println(noSuchElementException.getMessage());
        }
        return personDtos;
    }

    @Override
    public List<PersonDto> getPersonsByColorName(String name) {
        List<Person> personList;
        List<PersonDto> personDtos = new ArrayList<>();
        try {
            personList = this.persons
                    .stream()
                    .filter(p -> p.getColor().getName().equals(name))
                    .collect(Collectors.toList());
            personDtos = this.mapToListDto(personList);
        } catch (NoSuchElementException noSuchElementException) {
            System.out.println(noSuchElementException.getMessage());
        }
        return personDtos;
    }

    @Override
    public PersonDto createPerson(PersonDto personDto) {
        Color color = this.colorCsvRepository.findColorByName(personDto.getColor());
        if(color == null) {
            this.colorCsvRepository.save(personDto.getColor());
        }

        Person person = this.mapToPerson(personDto);
        Long maxId = this.persons.get(this.persons.size() - 1).getId();
        Long id = maxId + 1;
        person.setId(id);
        this.persons.add(person);
        return this.mapToDto(person);
    }

    @Override
    public PersonDto updatePerson(PersonDto personDto) {
        Color color = this.colorCsvRepository.findColorByName(personDto.getColor());
        if(color == null) {
            this.colorCsvRepository.save(personDto.getColor());
        }

        try {
            Person person = this.persons
                    .stream()
                    .filter(p -> p.getId().equals(personDto.getId()))
                    .findFirst()
                    .get();

            person.setName(personDto.getName());
            person.setLastname(personDto.getLastname());
            person.setZipcode(personDto.getZipcode());
            person.setCity(personDto.getCity());
            person.setColor(this.colorCsvRepository.findColorByName(personDto.getColor()));

            return this.mapToDto(person);
        } catch (NoSuchElementException noSuchElementException) {
            return null;
        }
    }

    @Override
    public PersonDto deletePerson(Long id) {
        try {
            Person person = this.persons
                    .stream()
                    .filter(p -> p.getId().equals(id))
                    .findFirst()
                    .get();
            this.persons.remove(person);
            return null;
        } catch (NoSuchElementException noSuchElementException) {
            System.out.println(noSuchElementException.getMessage());
            return new PersonDto();
        }
    }

    private PersonDto mapToDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.setId(person.getId());
        personDto.setName(person.getName());
        personDto.setLastname(person.getLastname());
        personDto.setZipcode(person.getZipcode());
        personDto.setCity(person.getCity());
        personDto.setColor(person.getColor().getName());
        return personDto;
    }

    private Person mapToPerson(PersonDto personDto) {
        Person person = new Person();
        person.setId(personDto.getId());
        person.setName(personDto.getName());
        person.setLastname(personDto.getLastname());
        person.setZipcode(personDto.getZipcode());
        person.setCity(personDto.getCity());
        person.setColor(this.colorCsvRepository.findColorByName(personDto.getColor()));
        return person;
    }

    private List<PersonDto> mapToListDto(List<Person> people) {
        List<PersonDto> personDtos = new ArrayList<>();
        PersonDto personDto;
        for (Person person: people) {
            personDto = this.mapToDto(person);
            personDtos.add(personDto);
        }
        return personDtos;
    }
}
