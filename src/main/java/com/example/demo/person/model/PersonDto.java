package com.example.demo.person.model;

public class PersonDto {

    private Long id;
    private String lastname;
    private String name;
    private String zipcode;
    private String city;
    private String color;

    public PersonDto() {
    }

    public PersonDto(String lastname, String name, String zipcode, String city, String color) {
        this();
        this.lastname = lastname;
        this.name = name;
        this.zipcode = zipcode;
        this.city = city;
        this.color = color;
    }

    public PersonDto(Long id, String lastname, String name, String zipcode, String city, String color) {
        this();
        this.id = id;
        this.lastname = lastname;
        this.name = name;
        this.zipcode = zipcode;
        this.city = city;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonDto personDto = (PersonDto) o;

        if (!id.equals(personDto.id)) return false;
        if (!lastname.equals(personDto.lastname)) return false;
        if (!name.equals(personDto.name)) return false;
        if (!zipcode.equals(personDto.zipcode)) return false;
        if (!city.equals(personDto.city)) return false;
        return color.equals(personDto.color);
    }
}
