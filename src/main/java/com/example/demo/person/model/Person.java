package com.example.demo.person.model;

import com.example.demo.color.model.Color;

import javax.persistence.*;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",
            nullable = false)
    private Long id;

    @Column(name = "name",
            nullable = false)
    private String name;

    @Column(name = "lastname",
            nullable = false)
    private String lastname;

    @Column(name = "zipcode",
            nullable = false)
    private String zipcode;

    @Column(name = "city",
            nullable = false)
    private String city;

    @ManyToOne()
    @JoinColumn(name = "color_id")
    private Color color = null;

    public Person() {
    }

    public Person(String name, String lastname, String zipcode, String city, Color color) {
        this();
        this.name = name;
        this.lastname = lastname;
        this.zipcode = zipcode;
        this.city = city;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
