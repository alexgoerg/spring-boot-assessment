package com.example.demo.person.controller;

import com.example.demo.person.model.PersonDto;
import com.example.demo.person.service.PersonCsvServiceImpl;
import com.example.demo.person.service.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    /*@Autowired
    private PersonServiceImpl personService;

    public PersonController(PersonServiceImpl personService) {
        this.personService = personService;
    }*/

    @Autowired
    private PersonCsvServiceImpl personService;

    public PersonController(PersonCsvServiceImpl personService) {
        this.personService = personService;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/persons",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<PersonDto>> getPersons() {
    // public int getPersons() {
        List<PersonDto> personDtos = this.personService.getPersons();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("message", "Erfolgreich gelesen.");

        return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).body(personDtos);
        // return personDtos.size();
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/person/id/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getPersonById(@PathVariable(name = "id") Long id) {
        PersonDto personDto = this.personService.getPersonById(id);
        HttpHeaders httpHeaders = new HttpHeaders();
        if (personDto == null) {
            httpHeaders.add("message", "Eine Person mit dieser ID wurde nicht gefunden.");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).headers(httpHeaders).build();
        } else {
            httpHeaders.add("message", "Erfolgreich gelesen.");
            return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).body((this.personService.getPersonById(id)));
        }
    }


    @RequestMapping(
            method = RequestMethod.GET,
            path = "/person/name/{name}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getPersonByName(@PathVariable(name = "name") String name) {
        List<PersonDto> personDtos = this.personService.getPersonsByName(name);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("message", "Erfolgreich gelesen.");
        return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).body(personDtos);
    }


    @RequestMapping(
            method = RequestMethod.GET,
            path = "/person/color/{name}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getPersonByColor(@PathVariable(name = "name") String name) {
        List<PersonDto> personDtos = this.personService.getPersonsByColorName(name);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("message", "Erfolgreich gelesen.");
        return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).body(personDtos);
    }


    @RequestMapping(
            method = RequestMethod.POST,
            path = "/person",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity createPerson(@RequestBody PersonDto personDto) {
        PersonDto personDtoCreated = this.personService.createPerson(personDto);
        HttpHeaders httpHeaders = new HttpHeaders();
        if (personDtoCreated == null) {
            httpHeaders.add("message", "Fehler beim Erstellen.");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).headers(httpHeaders).build();
        } else {
            httpHeaders.add("message", "Erfolgreich erstellt.");
            return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).body(personDtoCreated);
        }
    }


    @RequestMapping(
            method = RequestMethod.PUT,
            path = "/person",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity updatePerson(@RequestBody PersonDto personDto) {
        PersonDto personDtoUpdated = personService.updatePerson(personDto);
        HttpHeaders httpHeaders = new HttpHeaders();
        if (personDtoUpdated == null) {
            httpHeaders.add("message", "Fehler beim Ändern.");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).headers(httpHeaders).build();
        } else {
            httpHeaders.add("message", "Erfolgreich geändert.");
            return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).body(personDtoUpdated);
        }
    }

    @RequestMapping(
            method = RequestMethod.DELETE,
            path = "/person/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity deletePerson(@PathVariable(name = "id") Long id) {
        PersonDto personDtoDeleted = this.personService.deletePerson(id);
        HttpHeaders httpHeaders = new HttpHeaders();
        if (personDtoDeleted != null) {
            httpHeaders.add("message", "Fehler beim Löschen.");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).headers(httpHeaders).build();
        } else {
            httpHeaders.add("message", "Erfolgreich gelöscht.");
            return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).body(personDtoDeleted);
        }
    }
}
