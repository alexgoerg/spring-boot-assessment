package com.example.demo.person.repository;

import com.example.demo.color.repository.ColorRepository;
import com.example.demo.color.repository.ColorCsvRepository;
import com.example.demo.person.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Repository
public class PersonCsvRepository {

    @Autowired
    private ColorRepository colorRepository;

    public List<Person> findAll() {
        List<Person> personList = this.readCSV();
        return personList;
    }

    private List<Person> readCSV() {
        List<Person> persons = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File("sample-input.csv"));) {
            Long personId = 1L;
            String csvRow = "";
            while (scanner.hasNextLine()) {
                String csvRowCached = replaceWhenFirstOrLastCharacterIsSpace(scanner.nextLine());
                if (countOccurences(csvRowCached, ',') < 3
                        || csvRowCached.substring(csvRowCached.length() - 1).equals(",")) {
                    csvRow += csvRowCached;
                } else {
                    csvRow = csvRowCached;
                }
                if (countOccurences(csvRow, ',') == 3
                        && !csvRowCached.substring(csvRowCached.length() - 1).equals(",")) {
                    persons.add(getPersonAttributeValuesFromLine(csvRow, personId));
                    csvRow = "";
                    personId = personId + 1;
                }
            }
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return persons;
    }

    private Person getPersonAttributeValuesFromLine(String line, Long personId) {
        Person person = new Person();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");
            int counter = 0;
            while (rowScanner.hasNext()) {
                if (counter == 0) {
                    person.setId(personId);
                    person.setLastname(replaceWhenFirstOrLastCharacterIsSpace(rowScanner.next()));
                } else if (counter == 1) {
                    person.setName(replaceWhenFirstOrLastCharacterIsSpace(rowScanner.next()));
                } else if (counter == 2) {
                    String zipCodeAndCity[] = replaceWhenFirstOrLastCharacterIsSpace(rowScanner.next()).split(" ", 2);
                    person.setZipcode(replaceWhenFirstOrLastCharacterIsSpace(zipCodeAndCity[0]));
                    person.setCity(replaceWhenFirstOrLastCharacterIsSpace(zipCodeAndCity[1]));
                } else if (counter == 3) {
                    person.setColor(new ColorCsvRepository().findById(Long.parseLong(rowScanner.next().replaceAll("\\s+", ""))));
                }
                counter = counter + 1;
            }
        }
        return person;
    }

    private String replaceWhenFirstOrLastCharacterIsSpace(String text) {
        if (text.length() > 0) {
            if (text.substring(0, 1).equals(" ")) {
                text = text.substring(1);
                text = replaceWhenFirstOrLastCharacterIsSpace(text);
            } else if (text.substring(text.length() - 1, text.length()).equals(" ")) {
                text = text.substring(0, text.length() - 1);
                text = replaceWhenFirstOrLastCharacterIsSpace(text);
            }
        }
        return text;
    }

    private int countOccurences(String someString, char searchedChar) {
        int count = 0;
        for (int i = 0; i < someString.length(); i++) {
            if (someString.charAt(i) == searchedChar) {
                count++;
            }
        }
        return count;
    }
}
