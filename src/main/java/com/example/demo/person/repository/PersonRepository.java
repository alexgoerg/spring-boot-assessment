package com.example.demo.person.repository;

import com.example.demo.color.model.Color;
import com.example.demo.person.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    @Query("SELECT p FROM Person p WHERE p.name = ?1")
    List<Person> findPersonByName(String name);

    @Query("SELECT p FROM Person p WHERE p.color = ?1")
    List<Person> findPersonByColor(Color color);
}
